Getting Started
---------------

```sh
# clone it
git clone git@gitlab.com:solankivipul2301/job-search-front-end.git
cd job-search-front-end

# Install dependencies
npm install

# Start development live-reload server
PORT=3000 npm run dev

# Start production server:
PORT=8080 npm start

# Demo db available in db folder
```
