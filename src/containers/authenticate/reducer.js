import {
    CHANGE_AUTH,
    LOGIN_SUCCESS,
    LOGIN_FAILURE,
} from './constants';

const initialState = {
    error: null,
    auth: false,
    user: null,
};

export const authenticateReducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE_AUTH:
            return {
                ...state,
                auth: action.payload,
            };

        case LOGIN_SUCCESS:
            return {
                ...state,
                user: action.payload,
            };

        case LOGIN_FAILURE:
            return {
                ...state,
                error: action.payload,
            };

        default:
            return state;
    }
};

export default authenticateReducer;
