import axiosApi, { setAuthHeader } from '../../axiosApi';
import {
    CHANGE_AUTH,
    LOGIN_SUCCESS,
    LOGIN_FAILURE,
} from './constants';

const loginSuccess = payload => ({
    type: LOGIN_SUCCESS,
    payload,
});

const loginFailure = payload => ({
    type: LOGIN_FAILURE,
    payload,
});

const changeAuth = payload => ({
    type: CHANGE_AUTH,
    payload,
});

export const isLoggedIn = () => dispatch => {
    const token = localStorage.getItem('token');
    const user = localStorage.getItem('user');

    if (
        token && token !== null &&
        user && user !== null
    ) {
        setAuthHeader(token);
        dispatch(changeAuth(true));
        dispatch(loginSuccess(JSON.parse(user)));
    }
};

export const doLogin = (loginUser, history) => dispatch => {
    // axiosApi({
    //     method: 'post',
    //     url: '/users/login',
    //     data: loginUser
    // }).then(resp => {
    //     if(resp.data.success) {
    //         const { user, token } = resp.data;
    //         localStorage.setItem('token', token);
    //         localStorage.setItem('user', JSON.stringify(user));
    //
    //         setAuthHeader(token);
    //         dispatch(changeAuth(true));
    //         dispatch(loginSuccess(user));
    //         history.push('/');
    //     } else {
    //         dispatch(loginFailure(resp.data.error));
    //     }
    // }).catch(error => {
    //     dispatch(loginFailure(error.message));
    // });

    const user = {
        name: 'admin',
        username: 'admin',
        age: 20,
    };
    const token = 'dkyrjtfh54746FY3690346GHj43643Jy5438657h4364vkc3h523yjf23yfk2u3';
    localStorage.setItem('token', token);
    localStorage.setItem('user', JSON.stringify(user));

    setAuthHeader(token);
    dispatch(changeAuth(true));
    dispatch(loginSuccess(user));
    history.push('/');
};


export const logOut = () => dispatch => {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    dispatch(changeAuth(false));
};

export const register = (registerForm, history) => dispatch => {
    console.log('action register user: ', registerForm);

};
