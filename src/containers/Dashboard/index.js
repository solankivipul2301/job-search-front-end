import { connect } from "react-redux";
import React, { Component } from "react";
import { Row, Col, ListGroup, ListGroupItem } from 'reactstrap';
import { Link } from 'react-router-dom';

import Tags from '../../components/Tags';
import CreateUpdateJob from '../../components/CreateUpdateJob';

import requireAuth from '../authenticate';
import "react-toastify/dist/ReactToastify.css";

class dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  componentDidMount() {
  }

    renderDashboardComponent = () => {
        const slug = this.props.match.params.slug;
        if(slug === 'tags'){
            return ( <Tags/> )
        }

        if(slug === 'jobs'){
            return ( <CreateUpdateJob/> )
        }

        return (
            <React.Fragment>
                Welcome to dashboard!
            </React.Fragment>
        )
    };


    render() {
    return (
        <Row>
          <Col
              lg="2"
              md="2"
              sm="3"
              xs="12"
              className="dashboard-aside"
              style={{ minWidth: '190px' }}
          >
            <ListGroup>
              <ListGroupItem disabled>Dashboard</ListGroupItem>
                <ListGroupItem>
                  <Link to="/dashboard/tags">
                    Tags
                  </Link>
                </ListGroupItem>

                <ListGroupItem>
                  <Link to="/dashboard/jobs">
                  Jobs
                  </Link>
                </ListGroupItem>

            </ListGroup>

          </Col>

          <Col>
              {this.renderDashboardComponent()}
          </Col>

        </Row>

    );
  }
}

const mapDispatchToProps = () => {
  return {};
};
const mapStateToProps = state => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(dashboard);
// )(requireAuth(dashboard));
