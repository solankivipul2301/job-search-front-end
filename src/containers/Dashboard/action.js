import axiosApi from "../../axiosApi";
import {
    GET_TAG_LIST_SUCCESS,
    GET_JOB_LIST_SUCCESS,
} from './constants';

const getAllTagsSuccess = payload => ({
    type: GET_TAG_LIST_SUCCESS,
    payload
});
const getAllJobsSuccess = payload => ({
    type: GET_JOB_LIST_SUCCESS,
    payload
});

export const getAllTags = () => dispatch => {
    return axiosApi({
        method: 'GET',
        url: `/tags`
    }).then(resp => {
        if(resp && resp.data && resp.data.success) {
            dispatch(getAllTagsSuccess(resp.data.tags));
        }
    }).catch(error => {
        console.log({error});
    });
};

export const saveTags = (data) => dispatch => {
    return axiosApi({
        method: 'POST',
        url: `/tag`,
        data
    }).then(resp => {
        if(resp && resp.data && resp.data.success) {
            dispatch(getAllTags());
        }
    }).catch(error => {
        console.log({error});
    });
};

export const removeTags = (id) => dispatch => {
    return axiosApi({
        method: 'DELETE',
        url: `/tag/${id}`,
    }).then(resp => {
        if(resp && resp.data && resp.data.success) {
            dispatch(getAllTags());
        }
    }).catch(error => {
        console.log({error});
    });
};

export const updateTags = (data) => dispatch => {
    return axiosApi({
        method: 'PUT',
        url: `/tag/${data._id}`,
        data
    }).then(resp => {
        if(resp && resp.data && resp.data.success) {
            dispatch(getAllTags());
        }
    }).catch(error => {
        console.log({error});
    });
};

export const getAllJobs = () => dispatch => {
    return axiosApi({
        method: 'GET',
        url: `/jobs`
    }).then(resp => {
        if(resp && resp.data && resp.data.success) {
            dispatch(getAllJobsSuccess(resp.data.jobs));
        }
    }).catch(error => {
        console.log({error});
    });
};

export const saveJobs = (data) => dispatch => {
    return axiosApi({
        method: 'POST',
        url: `/job`,
        data
    }).then(resp => {
        if(resp && resp.data && resp.data.success) {
            dispatch(getAllJobs());
        }
    }).catch(error => {
        console.log({error});
    });
};

export const updateJobs = (data) => dispatch => {
    return axiosApi({
        method: 'PUT',
        url: `/job/${data._id}`,
        data
    }).then(resp => {
        if(resp && resp.data && resp.data.success) {
            dispatch(getAllJobs());
        }
    }).catch(error => {
        console.log({error});
    });
};
export const removeJobs = (id) => dispatch => {
    return axiosApi({
        method: 'DELETE',
        url: `/job/${id}`
    }).then(resp => {
        if(resp && resp.data && resp.data.success) {
            dispatch(getAllJobs());
        }
    }).catch(error => {
        console.log({error});
    });
};


