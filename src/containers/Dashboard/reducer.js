import {
    GET_TAG_LIST_SUCCESS,
    GET_JOB_LIST_SUCCESS,
} from './constants';

const initialState = {
    tagList: [],
    jobList: [],
};

export const dashboardReducer = ( state = initialState, action ) => {
    switch( action.type ) {
        case GET_TAG_LIST_SUCCESS:
            return {
                ...state,
                tagList: action.payload
            };

        case GET_JOB_LIST_SUCCESS:
            return {
                ...state,
                jobList: action.payload
            };

        default:
            return state;
    }
};

export default dashboardReducer;
