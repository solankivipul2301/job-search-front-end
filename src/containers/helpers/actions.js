import axiosApi from '../../axiosApi';
import {
    SEARCH_JOB_LIST_SUCCESS
} from './constants';

const searchedJobsSuccess = payload => ({
    type: SEARCH_JOB_LIST_SUCCESS,
    payload
});

export const searchJobs = (searchQuery) => dispatch => {
    return axiosApi({
        method: 'GET',
        url: `/search-jobs/${searchQuery}`
    })
    .then(resp => {
        console.log({resp});
        dispatch(searchedJobsSuccess(resp.data.searchedJobsList));
    })
    .catch(err => console.log({err}))
}