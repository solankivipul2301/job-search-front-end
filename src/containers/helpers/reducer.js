import {
    SEARCH_JOB_LIST_SUCCESS
} from './constants';

const initialState = {
    searchedJobsList: [
    ],
};

export const helpersReducer = ( state = initialState, action ) => {
    switch( action.type ) {
        case SEARCH_JOB_LIST_SUCCESS:
            return {
                ...state,
                searchedJobsList: action.payload
            };
        default:
            return {
                ...state,
            };
    }
};

export default helpersReducer;