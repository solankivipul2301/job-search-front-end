import { connect } from "react-redux";
import React, { Component } from "react";
import {
    Col, Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button, Row,
    FormGroup, Label, Input, Badge
} from 'reactstrap';
import map from 'lodash/map';
import sumBy from 'lodash/sumBy';

import "react-toastify/dist/ReactToastify.css";
import './home.css';
import { searchJobs } from '../helpers/actions';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
        searchQuery: ''
    };
  }
    componentDidMount() {
        this.props.onSearchJobs('all');
    }

    searchChangeHandler = e => {
        const searchQuery = e.target.value;
        this.setState({ searchQuery });
        const squery = searchQuery === '' ? 'all' : searchQuery;
        this.props.onSearchJobs(squery)

    };

  renderRank = item => {
      let rank = 0;
      rank = item.jobTags && item.jobTags.length > 0 &&
      sumBy(item.jobTags, jt => jt.rank);

      return rank;
  };

  render() {
    const { searchQuery } = this.state;
    const { searchedJobsList } = this.props;

    return (
        <Row>
            <Col md={12}>
                <FormGroup>
                    <Label for="jobSearch">Search</Label>
                    <Input
                        className="col-md-3"
                        type="search"
                        name="searchQuery"
                        id="jobSearch"
                        placeholder="job search"
                        value={searchQuery}
                        onChange={this.searchChangeHandler}
                    />
                </FormGroup>
            </Col>

            {searchedJobsList && searchedJobsList.map((item, itemIndex) =>
                <Col sm={3} key={itemIndex}>
                  <Card>
                    <CardBody>
                      <CardTitle>{item.title}</CardTitle>
                      <CardSubtitle>
                          {item.jobTags && item.jobTags.length > 0 &&
                              map(item.jobTags, jobTag =>
                                  <Badge color="secondary" className="m-r-5">
                                      {jobTag.tagName}
                                  </Badge>
                              )
                          }
                      </CardSubtitle>
                      <CardText>
                          {item.description && item.description.length > 90
                              ? `${item.description.substring(0, 90)}...` : item.description
                          }
                      </CardText>
                        <b>
                            Rank: {this.renderRank(item)}
                        </b>
                    </CardBody>
                  </Card>
                </Col>
            )}
        </Row>
    );
  }
}

const mapDispatchToProps = dispatch => ({
    onSearchJobs: (searchQuery) => dispatch(searchJobs(searchQuery)),
});

const mapStateToProps = state => ({
    searchedJobsList: state.helpers.searchedJobsList
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);