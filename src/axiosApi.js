import axios from 'axios';
import config from "./config";

const axiosApi = axios.create({
    baseURL: config.API_URL,
});

export const setAuthHeader = token => {
    axiosApi.defaults.headers.common.Authorization =
        `${token}` || `${localStorage.getItem('token')}`;
};

axiosApi.defaults.headers.common.Authorization = `${localStorage.getItem(
    'token',
)}`;

// Set the initial header from storage or something (should surround with try catch in actual app)
setAuthHeader(localStorage.getItem('token'));

export default axiosApi;
