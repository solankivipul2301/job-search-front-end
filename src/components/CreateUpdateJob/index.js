import { connect } from "react-redux";
import React, { Component } from "react";
import PropTypes from 'prop-types';
import ReactTable from 'react-table-6';
import { Multiselect } from 'multiselect-react-dropdown';
import map from 'lodash/map';
import pick from 'lodash/pick';


import { Row, Col, FormGroup, Label, Input, Button } from 'reactstrap';
import {
    getAllTags,
    getAllJobs,
    saveJobs,
    updateJobs,
    removeJobs
} from '../../containers/Dashboard/action';

const initialState = {
    selectedJob: {
        title: '',
        designation: '',
        location: '',
        qualifications: '',
        yearOfExp: '',
        description: '',
        tags: [],
    },
    isEdit: false,
    tagList: [],
};

class CreateUpdateJob extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
    }

    componentDidMount() {
        this.props.onGetAllJobs();
        this.props.onGetAllTags();
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps && nextProps.tagList !== this.props.tagList){
            const updatedTagList = map(nextProps.tagList, tag => pick(tag, '_id', 'name', 'rank'));
            this.setState({
                tagList: updatedTagList
            })
        }
    }

    onChangeHandler = (event) => {
        const {name, value} = event.target;

        this.setState({
            selectedJob: {
                ...this.state.selectedJob,
                [name]: value
            }
        });
    };

    onSubmitHandler = (event) => {
        const { selectedJob, isEdit } = this.state;

        if(isEdit) {
            this.props.onUpdateJobs(selectedJob);
        } else {
            this.props.onSaveJobs(selectedJob);
        }

        this.setState({
            selectedJob: initialState.selectedJob
        })
    };

    onMultiSelectChangeHandler = props => {
        this.setState({
            selectedJob: {
                ...this.state.selectedJob,
                tags: props
            }
        });
    };

    onEditHandler = props => {
        const { tagList } = this.state;
        const selectedJob =  props.original;

        let tags = [];
        map(tagList, tag => {
            return map(selectedJob.jobTags, jobTag => {
                if(jobTag.tagName === tag.name){
                    tags = [
                        ...tags,
                        { _id: tag._id, name:  tag.name, rank: tag.rank }
                    ]
                }
            });
        });

        this.setState({
            selectedJob : {
                ...selectedJob,
                tags
            },
            isEdit: true,
        });
    };

    onCancelHandler = props =>
        this.setState({
            selectedJob: initialState.selectedJob,
            isEdit: false,
        });

    onDeleteHandler = props => {
        if(window.confirm()) {
            this.props.onRemoveJobs(props.original._id);
        }
    };

    renderActionsButton = props => {
        return (
            <React.Fragment>
                <Button color="secondary" onClick={() => this.onEditHandler(props)}> Edit </Button>
                <Button color="danger" onClick={() => this.onDeleteHandler(props)}> Delete </Button>
            </React.Fragment>
        );
    };

    render() {
        const { selectedJob, isEdit, tagList } = this.state;
        const { jobList } = this.props;

        const columns = [
            {
                Header: 'Title',
                accessor: "title",
            },
            {
                Header: 'Location',
                accessor: "location",
            },
            {
                Header: 'Description',
                accessor: "description",
            },
            {
                Header: 'Tags',
                accessor: "jobTags",
                Cell: props => {
                    const jobTags = props.value;
                    let jobTagsData = '';
                    map(jobTags, jobTag => {
                        jobTagsData = `${jobTagsData}, ${jobTag.tagName}`
                    });

                    return jobTagsData && jobTagsData.length > 0 ? jobTagsData.substr(2) : jobTagsData;

                }
            },
            {
                Header: 'Action',
                accessor: 'id',
                Cell: this.renderActionsButton,
                className: "center"
            },
        ];


        return (
        <React.Fragment>
            <Row form>
                <Col md={6}>
                    <FormGroup>
                        <Label for="title">Title</Label>
                        <Input
                            type="text"
                            name="title"
                            id="title"
                            value={selectedJob.title}
                            onChange={this.onChangeHandler}
                        />
                    </FormGroup>
                </Col>
                <Col md={6}>
                    <FormGroup>
                        <Label for="designation">Designation</Label>
                        <Input
                            type="text"
                            name="designation"
                            id="designation"
                            value={selectedJob.designation}
                            onChange={this.onChangeHandler}
                        />
                    </FormGroup>
                </Col>
                <Col md={6}>
                    <FormGroup>
                        <Label for="location">Location</Label>
                        <Input
                            type="text"
                            name="location"
                            id="location"
                            value={selectedJob.location}
                            onChange={this.onChangeHandler}
                        />
                    </FormGroup>
                </Col>
                <Col md={6}>
                    <FormGroup>
                        <Label for="qualifications">Qualifications</Label>
                        <Input
                            type="text"
                            name="qualifications"
                            id="qualifications"
                            value={selectedJob.qualifications}
                            onChange={this.onChangeHandler}
                        />
                    </FormGroup>
                </Col>
                <Col md={6}>
                    <FormGroup>
                        <Label for="yearOfExp">Year Of Exp.</Label>
                        <Input
                            type="text"
                            name="yearOfExp"
                            id="yearOfExp"
                            value={selectedJob.yearOfExp}
                            onChange={this.onChangeHandler}
                        />
                    </FormGroup>
                </Col>
                <Col md={6}>
                    <FormGroup>
                        <Label for="description">Description</Label>
                        <Input
                            type="textarea"
                            name="description"
                            id="description"
                            value={selectedJob.description}
                            onChange={this.onChangeHandler}
                        />
                    </FormGroup>
                </Col>
                <Col md={6}>
                    <FormGroup>
                        <Label for="tags">Tags</Label>
                        <Multiselect
                            options={tagList}
                            selectedValues={selectedJob.tags}
                            onSelect={this.onMultiSelectChangeHandler}
                            onRemove={this.onMultiSelectChangeHandler}
                            displayValue="name"
                        />
                    </FormGroup>
                </Col>
                <Col md={12} className="text-center">
                    {isEdit ? (
                        <React.Fragment>
                            <Button
                                className="m-t-25 btn-success"
                                onClick={this.onSubmitHandler}
                            >
                                Update
                            </Button>
                            <Button
                                className="m-t-25 btn-secondary"
                                onClick={this.onCancelHandler}
                            >
                                Cancel
                            </Button>
                        </React.Fragment>
                    ) : (
                        <Button
                            className="m-t-25 btn-success"
                            onClick={this.onSubmitHandler}
                        >
                            Save
                        </Button>
                    )}

                </Col>
            </Row>
            <Row className="m-t-25">
                <Col>
                    {jobList && jobList.length > 0 &&
                    <ReactTable
                        minRows={0}
                        columns={columns}
                        data={jobList}
                        NoDataComponent={() => null}
                        pageSize = { jobList && jobList.length }
                        style={{
                            maxHeight: "calc(pageSize*35px + 10)" //100vh - 50px
                        }}
                    />
                    }
                </Col>
            </Row>
        </React.Fragment>
        );
    }
}

CreateUpdateJob.propsType = {
    onGetAllTags: PropTypes.func,
    onGetAllJobs: PropTypes.func,
    jobList: PropTypes.array,
    tagList: PropTypes.array,
    onSaveJobs: PropTypes.func,
    onUpdateJobs: PropTypes.func,
    onRemoveJobs: PropTypes.func,
};

const mapDispatchToProps = dispatch => ({
    onGetAllJobs: () => dispatch(getAllJobs()),
    onGetAllTags: () => dispatch(getAllTags()),
    onSaveJobs: (data) => dispatch(saveJobs(data)),
    onUpdateJobs: (data) => dispatch(updateJobs(data)),
    onRemoveJobs: (id) => dispatch(removeJobs(id)),
});

const mapStateToProps = state => ({
    jobList: state.dashboard.jobList,
    tagList: state.dashboard.tagList,
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CreateUpdateJob);
