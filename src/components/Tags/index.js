import { connect } from "react-redux";
import React, { Component } from "react";
import PropTypes from 'prop-types';
import ReactTable from 'react-table-6';

import { Row, Col, FormGroup, Label, Input, Button } from 'reactstrap';
import { getAllTags,saveTags, removeTags, updateTags } from '../../containers/Dashboard/action';

const initialState = {
    selectedTag: {
        name: '',
        rank: '',
    },
    isEdit: false,
};

class Tags extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
    }

    componentDidMount() {
        this.props.onGetAllTags();
    }

    onChangeHandler = (event) => {
        const {name, value} = event.target;

        this.setState({
            selectedTag: {
                ...this.state.selectedTag,
                [name]: value
            }
        });
    };

    onSubmitHandler = (event) => {
        const { selectedTag, isEdit } = this.state;
        if(isEdit) {
            this.props.onUpdateTags(selectedTag);
        } else {
            this.props.onSaveTags(selectedTag);
        }

        this.setState({
            selectedTag: initialState.selectedTag
        })
    };


    onEditHandler = props =>
        this.setState({
            selectedTag: props.original,
            isEdit: true,
        });

    onCancelHandler = props =>
        this.setState({
            selectedTag: initialState.selectedTag,
            isEdit: false,
        });

    onDeleteHandler = props => {
        if(window.confirm()) {
            this.props.onRemoveTags(props.original._id);
        }
    };

    renderActionsButton = props => {
        return (
            <React.Fragment>
                <Button color="secondary" onClick={() => this.onEditHandler(props)}> Edit </Button>
                <Button color="danger" onClick={() => this.onDeleteHandler(props)}> Delete </Button>
            </React.Fragment>
        );
    };

    render() {
        const { selectedTag, isEdit } = this.state;
        const { tagList } = this.props;

        const columns = [
            {
                Header: 'Name',
                accessor: "name",
            },
            {
                Header: 'Rank',
                accessor: "rank",
            },
            {
                Header: 'Action',
                accessor: 'id',
                Cell: this.renderActionsButton,
                className: "center"
            },
        ];


        return (
        <React.Fragment>
            <Row form>
                <Col md={4}>
                    <FormGroup>
                        <Label for="tag">Tag</Label>
                        <Input
                            type="text"
                            name="name"
                            id="tag"
                            value={selectedTag.name}
                            onChange={this.onChangeHandler}
                        />
                    </FormGroup>
                </Col>
                <Col md={2}>
                    <FormGroup>
                        <Label for="rank">Rank</Label>
                        <Input
                            type="text"
                            name="rank"
                            id="rank"
                            value={selectedTag.rank}
                            onChange={this.onChangeHandler}
                        />
                    </FormGroup>
                </Col>
                <Col md={6}>
                    {isEdit ? (
                        <React.Fragment>
                            <Button
                                className="m-t-25 btn-success"
                                onClick={this.onSubmitHandler}
                            >
                                Update
                            </Button>
                            <Button
                                className="m-t-25 btn-secondary"
                                onClick={this.onCancelHandler}
                            >
                                Cancel
                            </Button>
                        </React.Fragment>
                    ) : (
                        <Button
                            className="m-t-25 btn-success"
                            onClick={this.onSubmitHandler}
                        >
                            Save
                        </Button>
                    )}

                </Col>
            </Row>
            <Row>
                <Col>
                    {tagList && tagList.length > 0 &&
                    <ReactTable
                        minRows={0}
                        columns={columns}
                        data={tagList}
                        NoDataComponent={() => null}
                        pageSize = { tagList && tagList.length }
                        style={{
                            maxHeight: "calc(pageSize*35px + 10)" //100vh - 50px
                        }}
                    />
                    }
                </Col>
            </Row>
        </React.Fragment>
        );
    }
}

Tags.propsType = {
    onSaveTags: PropTypes.func,
    onGetAllTags: PropTypes.func,
    tagList: PropTypes.array,
    onUpdateTags: PropTypes.func,
    onRemoveTags: PropTypes.func,
};

const mapDispatchToProps = dispatch => ({
    onGetAllTags: () => dispatch(getAllTags()),
    onSaveTags: (data) => dispatch(saveTags(data)),
    onUpdateTags: (data) => dispatch(updateTags(data)),
    onRemoveTags: (id) => dispatch(removeTags(id)),
});

const mapStateToProps = state => ({
    tagList: state.dashboard.tagList,
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Tags);
