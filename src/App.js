import React, { Component } from 'react';
import { Route, Switch, BrowserRouter, Redirect } from 'react-router-dom';
import { Container, Row, Col } from 'reactstrap';
import './App.css';
// Styles
// Import Flag Icons Set
import 'flag-icon-css/css/flag-icon.min.css';
// Import Font Awesome Icons Set
import 'font-awesome/css/font-awesome.min.css';
// Import Simple Line Icons Set
import 'simple-line-icons/css/simple-line-icons.css';
// Import Main styles for this application
import './scss/style.css'
// import '../node_modules/@coreui/styles/scss/_dropdown-menu-right.scss';
import 'react-table-6/react-table.css'

// Containers
import {
  Home, Dashboard, NotFoundPage,
} from './containers';
import { Header } from './components';


// import { renderRoutes } from 'react-router-config';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <React.Fragment>
          <Header />
          <Container>
            <Row className="app">
              <Col>
                <Switch>
                  <Route exact path="/" name="Home" component={Home} />
                  <Route exact path="/dashboard" name="Dashboard" component={Dashboard} />
                  <Route exact path="/dashboard/:slug" name="Dashboard" component={Dashboard} />

                  <Route name="Page Not Found" component={NotFoundPage} />
                </Switch>
              </Col>
            </Row>
          </Container>
        </React.Fragment>
      </BrowserRouter>
    );
  }
}

export default App;
