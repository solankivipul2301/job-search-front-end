import { combineReducers } from 'redux';
import authenticateReducer from '../containers/authenticate/reducer';
import helpersReducer from '../containers/helpers/reducer';
import homeReducer from '../containers/Home/reducer';
import dashboardReducer from '../containers/Dashboard/reducer';

export default combineReducers({
    authenticate: authenticateReducer,
    helpers: helpersReducer,
    home: homeReducer,
    dashboard: dashboardReducer,
});
